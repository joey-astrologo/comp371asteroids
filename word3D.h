#ifndef word3d_H
#define word3D_H
#include <string>
//#include <gl\glew.h>     
#include <GL\freeglut.h>
#include <gl\glu.h>

using std::string;

class word3D
{
public:
	word3D(const string, float, float, float);
	~word3D() {delete wordTiles;};

	void setFileName(const string newFile) {fileName = newFile;}
	void drawWord();
private:
	string fileName;
	int height;
	int width;
	int** wordTiles;

	float red;
	float green;
	float blue;
};
#endif
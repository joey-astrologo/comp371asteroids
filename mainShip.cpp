#include "mainShip.h"
#include "randomNumbers.h"

using namespace randomNumbers;

unsigned int mainShip::loadObject()
{
	if (mainShip::idlist != 0)
		return mainShip::idlist;
	loader mesh("resources\\spaceship.obj");

	int i, v_ind, n_ind, t_ind;
	mainShip::idlist = glGenLists(1);
	glNewList(mainShip::idlist, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	for (i = 0; i < mesh.index_num; i++){
		v_ind = mesh.v_index[i];
		n_ind = mesh.n_index[i];
		t_ind = mesh.t_index[i];
		glNormal3f(mesh.normals[n_ind * 3], mesh.normals[n_ind * 3 + 1], mesh.normals[n_ind * 3 + 2]);
		glTexCoord2f(mesh.texture[t_ind * 2], mesh.texture[t_ind * 2 + 1]);
		glVertex3f(mesh.vertices[v_ind * 3], mesh.vertices[v_ind * 3 + 1], mesh.vertices[v_ind * 3 + 2]);
	}
	glEnd();
	glEndList();
	return mainShip::idlist;	
}

unsigned int mainShip::loadTexture(){
	if (mainShip::idtex != 0)
		return mainShip::idtex;
	mainShip::idtex = loader::loadTexture("resources\\ship.bmp");
	return mainShip::idtex;
}
unsigned int mainShip::idlist;
unsigned int mainShip::idtex;

mainShip::mainShip() : lives(5), tempInvincible(true), invinCounter(0), inertia(0.0f), drawBooster(false)
{
	position.x = 0.0f;
	position.y = 0.0f;
	degree = 0.0f;
	bSphere.x = 0.0f;
	bSphere.y = 0.0f;
	bSphere.z = 0.0f;
	bSphere.radius = 2;

	for(int i = 0; i < 50; i++)
	{
		particle curParticle;
		curParticle.life = 10;
		curParticle.age = 0;
		curParticle.color.red = 0.0f;
		curParticle.color.green = 1.0f;
		curParticle.color.blue = 1.0f;
		curParticle.pos = position;

		float x = -1.0f;
		float y = getRandomNumf(0.5, -0.5);

		
		curParticle.velocity.x =  x * 0.05f * getRandomNumf(1.0f, 0.0f);
   		curParticle.velocity.y = y * 0.05f * getRandomNumf(1.0f, 0.0f);
		booster.push_back(curParticle);
	}
}
mainShip::mainShip(float x, float y) : lives(5), tempInvincible(true), invinCounter(0), inertia(0.0f), drawBooster(false)
{
	position.x = x;
	position.y = y;
	degree = 0.0f;
	bSphere.x = x;
	bSphere.y = y;
	bSphere.z = 0.0f;
	bSphere.radius = 2;

	for(int i = 0; i < 50; i++)
	{
		particle curParticle;
		curParticle.life = 10;
		curParticle.age = 0;
		curParticle.color.red = 0.0f;
		curParticle.color.green = 1.0f;
		curParticle.color.blue = 1.0f;
		curParticle.pos = position;

		float x = -1.0f;
		float y = getRandomNumf(0.5, -0.5);

		
		curParticle.velocity.x =  x * 0.05f * getRandomNumf(1.0f, 0.0f);
   		curParticle.velocity.y = y * 0.05f * getRandomNumf(1.0f, 0.0f);
		booster.push_back(curParticle);
	}
}
void mainShip::draw()
{
	unsigned int list_vertex = mainShip::loadObject();
	unsigned int id_texture = mainShip::loadTexture();

	if(tempInvincible)
	{
		if(invinCounter % 10 == 0)
		{
			glPushMatrix();
				glTranslatef(position.x, position.y, 0.0f);
				glRotatef(degree, 0.0f, 0.0f, 1.0f);
				glScalef(0.3, 0.3, 0.3);
				glBindTexture(GL_TEXTURE_2D, id_texture);
				glColor3f(1.0f, 1.0f, 1.0f);
				glCallList(list_vertex);
				glBindTexture(GL_TEXTURE_2D, NULL);
			glPopMatrix();
		}
	}
	else
	{
		glPushMatrix();
			glTranslatef(position.x, position.y, 0.0f);
			glRotatef(degree, 0.0f, 0.0f, 1.0f);
			glScalef(0.3, 0.3, 0.3);
			glBindTexture(GL_TEXTURE_2D, id_texture);
			glColor3f(1.0f, 1.0f, 1.0f);
			glCallList(list_vertex);
			glBindTexture(GL_TEXTURE_2D, NULL);
		glPopMatrix();
	}

	if(drawBooster)
	{
		glPushMatrix();
			glTranslatef(position.x, position.y, 0.0f);
			glRotatef(degree, 0.0f, 0.0f, 1.0f);
			glBegin(GL_LINES);
				for(vector<particle>::iterator i = booster.begin(); i != booster.end(); i++)
				{
 					glColor3f((*i).color.red, (*i).color.green, (*i).color.blue);
				
					float x1 = (*i).pos.x + (*i).velocity.x * 200.0f;
					float y1 = (*i).pos.y + (*i).velocity.y * 200.0f;

					float x2 = (*i).pos.x;
					float y2 = (*i).pos.y;

					glVertex3f(x1, y1, 0.0f);
					glVertex3f(x2 , y2, 0.0f);
				}
			glEnd();
		glPopMatrix();
	}
}
void mainShip::update(int gameTime, bool* keyStates)
{
	float moveSpeed = 0.06f;
	float turnSpeed = 0.2f;

	vectorPosition direction;
	direction.x = cos(degree * (PI/180));
	direction.y = sin(degree * (PI/180));

	//update position
	if(keyStates['w'] == true)
	{
		if(inertia <= 0.06f)
			inertia += moveSpeed * 0.03f;

		drawBooster = true;
		//position.x += gameTime * moveSpeed * direction.x;
		//position.y += gameTime * moveSpeed * direction.y;
	}
	if(keyStates['w'] == false)
	{
		inertia -= moveSpeed * 0.0003f;
		if(inertia  <= 0.0f)
			inertia = 0;

		if(inertia > 0.06f)
			inertia = 0.06f;

		drawBooster = false;
		//position.x += gameTime * moveSpeed * direction.x;
		//position.y += gameTime * moveSpeed * direction.y;
	}
	/*if(keyStates['s'] == true)
	{
		position.x -= gameTime * moveSpeed * direction.x;
		position.y -= gameTime * moveSpeed * direction.y;
	}*/
	if(keyStates['a'] == true)
	{
		degree += gameTime * turnSpeed;
	}
	if(keyStates['d'] == true)
	{
		degree -= gameTime * turnSpeed;
	}

	position.x += gameTime * inertia * direction.x;
	position.y += gameTime * inertia * direction.y;

	//update viewport bounds
	if(position.x > 155.0f || position.x < -155.0f)
	{
		position.x *= -1.0f;
	}
	if(position.y > 90.0f || position.y < -90.0f)
	{
		position.y *= -1.0f;
	}

	//update bounding sphere
	bSphere.x = position.x;
	bSphere.y = position.y;

	if(tempInvincible)
	{
		invinCounter += gameTime;
	}
	if(invinCounter >= 600)
	{
		invinCounter = 0;
		tempInvincible = false;
	}


	for(int i = 0; i < 50; i++)
	{
		//update position
		booster[i].pos.x += gameTime * booster[i].velocity.x;
		booster[i].pos.y += gameTime * booster[i].velocity.y;

		booster[i].age += gameTime;

	

		if(booster[i].age >= booster[i].life)
		{
			booster[i].age = 0;
			booster[i].pos.x = 0.0f;
			booster[i].pos.y = 0.0f;


			float x = -1.0f;
			float y = getRandomNumf(0.5, -0.5);

			booster[i].velocity.x =  x * 0.05f * getRandomNumf(1.0f, 0.0f);
   			booster[i].velocity.y = y * 0.05f * getRandomNumf(1.0f, 0.0f);
		}
	}
}
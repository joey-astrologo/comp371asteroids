#include "randomNumbers.h"
#include <limits.h>
#include <cstdio>

int randomNumbers::getRandomNum(int to, int from) 
{
	return rand() % to + from; 
}

float randomNumbers::getRandomNumf(float to, float from)
{
	return from + static_cast<float> (rand()) / static_cast<float> (RAND_MAX / (to - from));
}
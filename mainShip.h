#ifndef mainShip_H
#define mainShip_H

#include <math.h>
#include <vector>

#include "gameEntity.h"
#include "loader.h"

using std::vector;

#define PI 3.14159265

class mainShip : public gameEntity
{
public:
	mainShip();

	mainShip(float x, float y);

	int lives;

	void draw();
	void update(int, bool*);

	vectorPosition getPosition() { return position; }
	float getDirection() { return degree; }
	void collided()
	{ 
		tempInvincible = true;
		Mix_PlayChannel( -1, sndCollision, 0 );
	}
	bool collidable() { return !tempInvincible; }

private:
	float inertia;
	float degree;
	bool tempInvincible;
	bool drawBooster;
	int invinCounter; 

	vector<particle> booster;

	static unsigned int loadTexture();
	static unsigned int loadObject();
	static unsigned int idlist;
	static unsigned int idtex;
};

#endif

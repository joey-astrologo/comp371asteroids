#include "titleScene.h"
#include "starsBackground.h"
titleScene::titleScene()
{
	title = new word3D("resources\\asteroid.dat", 0.0f, 1.0f, 0.0f);
	pressStart = new word3D("resources\\pressStart.dat", 0.0f, 1.0f, 0.0f);
	background = new starsBackground();
	music = Mix_LoadMUS("resources\\Featherhall.ogg");
	Mix_PlayMusic(music, -1);
}

titleScene::~titleScene()
{
	delete title;
	delete background;
	delete pressStart;

	//Mix_FreeMusic(music);
}
void titleScene::draw()
{
	background->draw();

	glPushMatrix();
		glTranslatef(-70.0, 15.0, 0.0);
		glScalef(3.0, 3.0, 3.0);
		title->drawWord();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(-25.0, -30.0, 0.0);
		pressStart->drawWord();
	glPopMatrix();

}

void titleScene::update(int /*gameTime*/, bool* /*keyState*/)
{



}


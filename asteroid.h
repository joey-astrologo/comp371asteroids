#ifndef asteroid_H
#define asteroid_H

#include <math.h>

#include "gameEntity.h"
#include "randomNumbers.h"
#include "loader.h"

using namespace randomNumbers;

enum size {NONE, SMALL, MEDIUM, BIG};

const int MAX_DEGREE = 330;

#define PI 3.14159265

class asteroid : public gameEntity
{
public:
	asteroid();
	asteroid(float x, float y, size);
	asteroid(float x, float y, size, float);
	~asteroid();

	void draw();
	void update(int, bool*);

	void setSize(const size newSize);
	void setDirection(const float newDirection) { dDirection *= -1.0f; }
	
	size getSize() { return currentSize; } const
	vectorPosition getPosition() { return position; } const

	void collided() { tempInvincible = true; }
	bool collidable() { return !tempInvincible; }
private:
	float degree;
	float dDirection;
	bool tempInvincible;
	int invinCounter;
	float ratio;
	float rotation_x, rotation_y, rotation_z;
	float moveSpeed;
	float turnSpeed;
	size currentSize;

	static unsigned int loadObject();
	static unsigned int loadTexture();
	static unsigned int idlist;
	static unsigned int idtex;

};
#endif
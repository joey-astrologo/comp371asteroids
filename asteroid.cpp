#include "asteroid.h"
#include <iostream>

unsigned int asteroid::loadObject(){
	if (asteroid::idlist != 0)
		return asteroid::idlist;
	loader mesh("resources\\asteroidMesh.obj");

	int i, v_ind, n_ind, t_ind;
	asteroid::idlist = glGenLists(1);
	glNewList(asteroid::idlist, GL_COMPILE);
	glBegin(GL_TRIANGLES);
	for (i = 0; i < mesh.index_num; i++){
		v_ind = mesh.v_index[i];
		n_ind = mesh.n_index[i];
		t_ind = mesh.t_index[i];
		glNormal3f(mesh.normals[n_ind * 3], mesh.normals[n_ind * 3 + 1], mesh.normals[n_ind * 3 + 2]);
		glTexCoord2f(mesh.texture[t_ind * 2], mesh.texture[t_ind * 2 + 1]);
		glVertex3f(mesh.vertices[v_ind * 3], mesh.vertices[v_ind * 3 + 1], mesh.vertices[v_ind * 3 + 2]);
	}
	glEnd();
	glEndList();
	return asteroid::idlist;
}

unsigned int asteroid::loadTexture(){
	if (asteroid::idtex != 0)
		return asteroid::idtex;
	asteroid::idtex = loader::loadTexture("resources\\asteroidTexture.bmp");
	return asteroid::idtex;
}

unsigned int asteroid::idlist = 0;
unsigned int asteroid::idtex = 0;

asteroid::~asteroid(){
	//glDeleteLists(list_id, 1);
}

asteroid::asteroid() 
{
	int tempDirection = 0;
	do
	{
		tempDirection = getRandomNum(MAX_DEGREE, 30);
	}while(tempDirection > 150 && tempDirection < 210);

	dDirection = (float)tempDirection;
	position.x = 0.0f;
	position.y = 0.0f;
	degree = 0.0f;
	bSphere.x = 0.0f;
	bSphere.y = 0.0f;
	bSphere.z = 0.0f;
	bSphere.radius = BIG;
	currentSize = BIG;
	ratio = 1.0;
	tempInvincible = false;
	invinCounter = 0;

	rotation_x = getRandomNumf(1.0, 0.0);
	rotation_y = getRandomNumf(1.0, 0.0);
	rotation_z = getRandomNumf(1.0, 0.0);
	turnSpeed = getRandomNumf(0.3, 0.0);
	moveSpeed = getRandomNumf(0.08, 0.03);
}
asteroid::asteroid(float x, float y, size newSize) 
{
    int tempDirection = 0;
	do
	{
		tempDirection = getRandomNum(MAX_DEGREE, 30);
	}while(tempDirection > 150 && tempDirection < 210);

	dDirection = (float)tempDirection;
	position.x = x;
	position.y = y;
	degree = 0.0f;
	bSphere.x = x;
	bSphere.y = y;
	bSphere.z = 0.0f;
	bSphere.radius = newSize;
	currentSize = newSize;
	switch(newSize)
	{
	case BIG:
		ratio = 0.6;
		break;
	case MEDIUM:
		ratio = 0.4;
		break;
	case SMALL:
		ratio = 0.2;
		break;
	}
	tempInvincible = false;
	invinCounter = 0;

	rotation_x = getRandomNumf(1.0, 0.0);
	rotation_y = getRandomNumf(1.0, 0.0);
	rotation_z = getRandomNumf(1.0, 0.0);
	turnSpeed = getRandomNumf(0.3, 0.0);
	moveSpeed = getRandomNumf(0.08, 0.03);
}
asteroid::asteroid(float x, float y, size newSize, float newDirection) 
{
    dDirection = newDirection;
	position.x = x;
	position.y = y;
	degree = 0.0f;
	bSphere.x = x;
	bSphere.y = y;
	bSphere.z = 0.0f;
	bSphere.radius = newSize;
	currentSize = newSize;
	tempInvincible = false;
	invinCounter = 0;
	switch(newSize)
	{
	case BIG:
		ratio = 0.6;
		break;
	case MEDIUM:
		ratio = 0.4;
		break;
	case SMALL:
		ratio = 0.2;
		break;
	}

	rotation_x = getRandomNumf(1.0, 0.0);
	rotation_y = getRandomNumf(1.0, 0.0);
	rotation_z = getRandomNumf(1.0, 0.0);
	turnSpeed = getRandomNumf(0.3, 0.0);
	moveSpeed = getRandomNumf(0.08, 0.03);
}
void asteroid::draw()
{
	unsigned int list_vertex = asteroid::loadObject();
	unsigned int id_texture = asteroid::loadTexture();
	glPushMatrix();
		glTranslatef(position.x, position.y, 0.0f);
		glRotatef(degree, rotation_x, rotation_y, rotation_z);
		glPushMatrix();
			glScalef(ratio, ratio, ratio);
			glColor3f(0.4f, 0.4f, 0.4f);
			glBindTexture(GL_TEXTURE_2D, id_texture);
			glCallList(list_vertex);
			glBindTexture(GL_TEXTURE_2D, NULL);
		glPopMatrix();
	glPopMatrix();

}
void asteroid::update(int gameTime, bool* /*keyStates*/)
{

	vectorPosition direction;
	direction.x = cos(dDirection * (PI/180));
	direction.y = sin(dDirection * (PI/180));

	//update position
	
	position.x += gameTime * moveSpeed * direction.x;
	position.y += gameTime * moveSpeed * direction.y;
	
	degree += gameTime * turnSpeed;
	
	//update viewport bounds
	if(position.x > 155.0f || position.x < -155.0f)
	{
		position.x *= -1.0f;
	}
	if(position.y > 90.0f || position.y < -90.0f)
	{
		position.y *= -1.0f;
	}

	//update bounding sphere
	bSphere.x = position.x;
	bSphere.y = position.y;

	if(tempInvincible)
	{
		invinCounter += gameTime;
	}
	if(invinCounter >= 600)
	{
		invinCounter = 0;
		tempInvincible = false;
	}
}
void asteroid::setSize(size newSize)
{
	currentSize = newSize;
}
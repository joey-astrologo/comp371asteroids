#include <string>

#ifndef loader_H
#define loader_H

class loader
{
public:
	loader(std::string filename);
	~loader();
	static GLuint loadTexture(std::string filename);

	int *v_index, *n_index, *t_index;
	float *vertices, *normals, *texture;
	size_t index_num, vertices_num, normal_num, texture_num;
};
#endif // !1




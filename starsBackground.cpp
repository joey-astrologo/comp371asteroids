#include "starsBackground.h"
#include "randomNumbers.h"
#include "loader.h"
using namespace randomNumbers;

starsBackground::starsBackground(/*int number*/)
{
	idTexture = loader::loadTexture("resources\\background.bmp");



	/*

	for (int i = 0; i < number; i++)
	{
	vectorPosition v;
	v.x = (getRandomNumf(1.0f, -1.0f)) * 150;
	v.y = (getRandomNumf(1.0f, -1.0f)) * 150;
	starsPositions.push_back(v);
	}
	*/
}


void starsBackground :: draw()
{
	float x = 170.0f;
	float y = 130.0f;
	float z = -10.0f;

	glBindTexture(GL_TEXTURE_2D, idTexture);
	glPushMatrix();
		glBegin(GL_QUADS);
			glColor3f(0.2f, 0.2f, 0.2f);
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(x, y, z);
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(x, -y, z);
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-x, -y, z);
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-x, y, z);
		glEnd();
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, NULL);
	/*
	glPushMatrix();
		glTranslatef(0.0f, 0.0f, -10.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glPointSize(2.0f);
		glBegin(GL_POINTS);
			for (int i = 0; i < starsPositions.size(); i++)
			{
				glVertex3f(starsPositions[i].x, starsPositions[i].y, 0.0f);
			}
		glEnd();


		glColor4f(1.0f, 1.0f, 1.0f, 0.2f);
		glPointSize(5.0f);
		glBegin(GL_POINTS);
			for (int i = 0; i < starsPositions.size(); i++)
			{
				glVertex3f(starsPositions[i].x, starsPositions[i].y, 0.0f);
			}
		glEnd();
	glPopMatrix();
	*/
}


void starsBackground :: update(int, bool*)
{
}
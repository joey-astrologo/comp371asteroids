#ifndef randomNumbers_H
#define randomNumbers_H

#include <GL\freeglut.h> 
#include <stdlib.h>
#include <stdio.h>

namespace randomNumbers
{
	int getRandomNum(int to, int from);
	float getRandomNumf(float to, float from);
};

#endif
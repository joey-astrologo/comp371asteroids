#include "displayText.h"

void displayText::enter2D()
{
	glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT), 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    glDisable(GL_DEPTH_TEST);
}
void displayText::leave2D()
{
    glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
}
void displayText::writeText(void *font, string text, int x, int y )
{
	glRasterPos2i( x, y );
	for(int i = 0; i < text.length(); i++)
	{
		glutBitmapCharacter(font, text[i]);
	}
}
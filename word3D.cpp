#include "word3D.h"
#include <fstream>

using std::ifstream;


word3D::word3D(const string newFile, float newRed, float newGreen, float newBlue) : fileName(newFile), red(newRed), green(newGreen), blue(newBlue)
{
	string line;
	ifstream myfile (fileName.c_str());
	if (myfile.is_open())
	{
		getline(myfile, line);
		width = atoi(line.c_str());
		getline(myfile, line);
		height = atoi(line.c_str());
		wordTiles = new int*[height];
		char outPut;
		for(int x = 0; x < height; x++)
		{
			wordTiles[x] = new int[width];
			for(int y = 0; y < width; y++)
			{
				outPut = myfile.get();
				wordTiles[x][y] = outPut - 48;
			}
			myfile.get();
		}
		
	}
}

void word3D::drawWord()
{
	
	for(int x = 0; x < height; x++)
	{
		for(int y = 0; y < width; y++)
		{
			if(wordTiles[x][y] == 1)
			{
				glPushMatrix();
						glTranslatef((float) y, (float) -x, 2.0f);
						
						glBegin(GL_QUADS);
						glColor3f(0.0f, 0.0f, 0.0f);
						glVertex3f( 1.0f, 1.0f,-1.0f);
						glVertex3f(-1.0f, 1.0f,-1.0f);
						glVertex3f(-1.0f, 1.0f, 1.0f);
						glVertex3f( 1.0f, 1.0f, 1.0f);

						glColor3f(red, green, blue);
						glVertex3f( 0.8f, 0.8f,-0.8f);
						glVertex3f(-0.8f, 0.8f,-0.8f);
						glVertex3f(-0.8f, 0.8f, 0.8f);
						glVertex3f( 0.8f, 0.8f, 0.8f);
						//bottom face
						glColor3f(0.0f, 0.0f, 0.0f);
						glVertex3f( 1.0f,-1.0f, 1.0f);
						glVertex3f(-1.0f,-1.0f, 1.0f);
						glVertex3f(-1.0f,-1.0f,-1.0f);
						glVertex3f( 1.0f,-1.0f,-1.0f);

						glColor3f(red, green, blue);
						glVertex3f( 0.8f,-0.8f, 0.8f);
						glVertex3f(-0.8f,-0.8f, 0.8f);
						glVertex3f(-0.8f,-0.8f,-0.8f);
						glVertex3f( 0.8f,-0.8f,-0.8f);
						//front face
						glColor3f(1.0f, 0.0f, 0.0f);
						glVertex3f( 1.0f, 1.0f, 1.0f);
						glVertex3f(-1.0f, 1.0f, 1.0f);
						glVertex3f(-1.0f,-1.0f, 1.0f);
						glVertex3f( 1.0f,-1.0f, 1.0f);
						glColor3f(red, green, blue);
						glVertex3f( 0.8f, 0.8f, 1.1f);
						glVertex3f(-0.8f, 0.8f, 1.1f);
						glVertex3f(-0.8f,-0.8f, 1.1f);
						glVertex3f( 0.8f,-0.8f, 1.1f);
						//back face
						glColor3f(0.0f, 0.0f, 0.0f);
						glVertex3f( 1.0f,-1.0f,-1.0f);
						glVertex3f(-1.0f,-1.0f,-1.0f);
						glVertex3f(-1.0f, 1.0f,-1.0f);
						glVertex3f( 1.0f, 1.0f,-1.0f);
						glColor3f(red, green, blue);
						glVertex3f( 0.8f,-0.8f,-0.8f);
						glVertex3f(-0.8f,-0.8f,-0.8f);
						glVertex3f(-0.8f, 0.8f,-0.8f);
						glVertex3f( 0.8, 0.8f,-0.8f);
						//left side
						glColor3f(0.0f, 0.0f, 0.0f);
						glVertex3f(-1.0f, 1.0f, 1.0f);
						glVertex3f(-1.0f, 1.0f,-1.0f);
						glVertex3f(-1.0f,-1.0f,-1.0f);
						glVertex3f(-1.0f,-1.0f, 1.0f);
						glColor3f(red, green, blue);
						glVertex3f(-0.8, 0.8f, 0.8f);
						glVertex3f(-0.8f, 0.8f,-0.8f);
						glVertex3f(-0.8f,-0.8f,-0.8f);
						glVertex3f(-0.8f,-0.8f, 0.8f);
						//right side
						glColor3f(0.0f, 0.0f, 0.0f);
						glVertex3f( 1.0f, 1.0f,-1.0f);
						glVertex3f( 1.0f, 1.0f, 1.0f);
						glVertex3f( 1.0f,-1.0f, 1.0f);
						glVertex3f( 1.0f,-1.0f,-1.0f);
						glColor3f(red, green, blue);
						glVertex3f( 0.8f, 0.8f,-0.8f);
						glVertex3f( 0.8f, 0.8f, 0.8f);
						glVertex3f( 0.8f,-0.8f, 0.8f);
						glVertex3f( 0.8f,-0.8f,-0.8f);
						glEnd();
				glPopMatrix();
					
			}
		}
	}
	
}
#ifndef gameEntity_H
#define gameEntity_H

#include <gl\glew.h> 
#include <GL\freeglut.h>
#include <gl\glu.h>	

#include <SDL\SDL_mixer.h>

struct vectorPosition
{
	float x;
	float y;
};
struct boundingSphere
{
	float x;
	float y;
	float z;
	int radius;
};

struct rgb
{
	float red;
	float green;
	float blue;
};

struct particle
{
	int life;
	int age;
	vectorPosition pos;
	vectorPosition velocity;
	rgb color;
};


class gameEntity
{
public:
	gameEntity() 
	{
		sndBullet = Mix_LoadWAV( "resources\\laser2b.wav" );
		sndExplosion  = Mix_LoadWAV( "resources\\Explosion.wav" );
	    sndCollision = Mix_LoadWAV( "resources\\Collision.wav" );
	};

	~gameEntity()
	{
		Mix_FreeChunk( sndBullet );
		Mix_FreeChunk( sndExplosion );
		Mix_FreeChunk( sndCollision );
	}

	virtual void draw() = 0;
	virtual void update(int, bool*) = 0;

	vectorPosition position;
	boundingSphere bSphere;
protected:
	Mix_Chunk* sndBullet;
	Mix_Chunk* sndExplosion;
	Mix_Chunk* sndCollision;
};

#endif
#ifndef title_H
#define title_H
#include <SDL\SDL_mixer.h>

#include "starsBackground.h"
#include "scene.h"
class titleScene : public scene
{

public:

	titleScene();
	~titleScene();
	void draw();
	void update(int, bool*);
private:

	word3D* title;
	word3D* pressStart;
	starsBackground* background;
	Mix_Music* music;
};
#endif
#ifndef framesPerSecond_H
#define framesPerSecond_H
#include <GL\freeglut.h>
#include <iostream>
#include <sstream>

#include "displayText.h"

using std::string;
using std::stringstream;
using namespace displayText;



namespace framesPerSecond
{
	extern int         g_nFPS;
	extern int         g_nFrames;		// FPS and FPS Counter
	extern DWORD       g_dwLastFPS;

	void calculateFPS();
	void drawFPS();
}
#endif


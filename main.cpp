/******************************************************************
Asteroids game
viewport in 720p
+/- 150.0f 0n x axis
+/- 85.0f on y axis

********************************************************************/
#include <gl\glew.h>
#include <GL\freeglut.h>
//#include <gl\gl.h>                   
#include <gl\glu.h>	
#include <math.h>
#include <SDL\SDL.h>
#include <SDL\SDL_mixer.h>

#include "framesPerSecond.h"
#include "gameScene.h"
#include "titleScene.h"

using framesPerSecond::drawFPS;

enum gameState {TITLE, GAME, OVER};

scene* currentScene;

gameState currentState = TITLE;

bool* keyStates = new bool[256];      //so we can handle multiple keys at once

int previousTime;					 //for game time
int currentTime;
int elapsedTime;

void resize(int width, int height)
{
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (GLfloat)width/(GLfloat)height, 1.0f, 1000.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 150.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	currentScene->draw();
	drawFPS();

    glutSwapBuffers();
}

void key(unsigned char key, int x, int y)
{
	keyStates[key] = true;
	switch(key)
	{
	case 13:
		if(currentState == TITLE)
		{
			delete currentScene;
			currentScene = new gameScene;
			currentState = GAME;
		}
		break;
	case 27 :
		Mix_CloseAudio();
		SDL_Quit();
		glutLeaveGameMode();
		exit(0);
		break;
	default:
		break;
	}
}
void keyUp(unsigned char key, int x, int y)
{
	keyStates[key] = false;
}

void specialKeyInput(int key, int x, int y)
{

}

void update()
{
    previousTime = currentTime;
    // Get the current time (in milliseconds) and calculate the elapsed time
    currentTime = glutGet(GLUT_ELAPSED_TIME);
    elapsedTime = currentTime - previousTime;
	//update game elements here
	currentScene->update(elapsedTime, keyStates);

    glutPostRedisplay();
}

void setup() 
{
    glClearColor(0.1, 0.1, 0.1, 0.0);  
    glutSetCursor(GLUT_CURSOR_NONE);
    glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2,
					glutGet(GLUT_WINDOW_HEIGHT) / 2);

	SDL_Init(SDL_INIT_AUDIO);

    Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 1024 );

	currentScene = new titleScene;
	//currentScene->setup();

	currentState = TITLE;
	currentTime = glutGet(GLUT_ELAPSED_TIME);

	srand(glutGet(GLUT_ELAPSED_TIME));
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(1280, 720);
    glutInitWindowPosition(10,10);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );

	glutGameModeString("1280x720:32");	
	glutEnterGameMode();//Game must be in 720p or else game elements might act oddly
	int checkGameMode = glutGameModeGet (GLUT_GAME_MODE_ACTIVE);

	if (checkGameMode != 0) 
	{
		glutLeaveGameMode();
		glutCreateWindow("Asteroids");
	}

	//glewInit();
	glEnable(GL_TEXTURE_2D);

	glutReshapeFunc(resize);

    glutDisplayFunc(display);
    glutKeyboardFunc(key);
	glutKeyboardUpFunc(keyUp);
	glutSpecialFunc(specialKeyInput);
	//glutMouseFunc(mouseButton);
	//glutPassiveMotionFunc(mouseMove);
    glutIdleFunc(update);

	setup();

    glutMainLoop();

	return 0;
}
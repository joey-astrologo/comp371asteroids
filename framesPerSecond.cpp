#include "framesPerSecond.h"
namespace framesPerSecond
{
	int         g_nFPS = 0;
	int         g_nFrames = 0;		
	DWORD      g_dwLastFPS;
}

void framesPerSecond::calculateFPS()
{
	if( glutGet(GLUT_ELAPSED_TIME) - g_dwLastFPS >= 1000 )               // When A Second Has Passed
    {
        g_dwLastFPS = glutGet(GLUT_ELAPSED_TIME);  
        g_nFPS = g_nFrames;												// Save The FPS
        g_nFrames = 0;													// Reset The FPS Counter
    }
    g_nFrames++;
}

void framesPerSecond::drawFPS()
{
	glPushMatrix();
		calculateFPS();

		enter2D();
		stringstream output;
		output << g_nFPS;
		glColor3f(1.0f, 0.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18,"FPS: ", 20, 20);
		glColor3f(0.0f, 1.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18, output.str(), 80, 20);
		leave2D();
	glPopMatrix();
}
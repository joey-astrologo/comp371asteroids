#ifndef bullet_H
#define bullet_H

#include <math.h>

#include "gameEntity.h"


#define PI 3.14159265

const int LIFE_TIME = 2000;

class bullet : public gameEntity
{
public:
	bullet(int, int, float);

	void draw();
	void update(int, bool*);

	bool getKillState() { return killBullet; }

private:
	float degree;
	int lifeTimeCounter;
	bool killBullet;
};
#endif
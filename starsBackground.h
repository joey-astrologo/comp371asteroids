
#ifndef starsBackground_H
#define starsBackground_H

#include "gameEntity.h"
#include <vector>
using std::vector;

class starsBackground : public gameEntity
{
private:
	//vector<vectorPosition> starsPositions;
	unsigned int idTexture;
public:
	starsBackground(/*int number*/);
	void draw();
	void update(int, bool*);
};

#endif
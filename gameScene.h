#ifndef gameScene_H
#define gameScene_H

#include <math.h>
#include <vector>
#include <algorithm>

#include "mainShip.h"
#include "randomNumbers.h"
#include "asteroid.h"
#include "displayText.h"
#include "bullet.h"
#include "explosion.h"
#include "starsBackground.h"
#include "scene.h"

using std::vector;
using std::find;
using namespace randomNumbers;
using namespace displayText;

const int MAX_X_AXIS = 150;
const int MAX_Y_AXIS = 85;
const int MAX_ASTEROIDS = 20;

class gameScene : public scene
{
public:

	gameScene();
	~gameScene();

	void draw();
	void update(int, bool*);

	bool setup();

private:
	starsBackground* background;
	mainShip* ship;
	asteroid* currentAsteroid;
	bullet*   currentBullet;
	explosion* currentExplosion;

	int spawnTime;
	int pauseShot;
	int collisionCount;
	int time;
	int gamePlayTimer;
	int timer;
	int score;

	bool gameOver;

	word3D* one;
	word3D* two;
	word3D* three;
	word3D* start;
	word3D* gameOverW;

	vector<asteroid*> asteroid_vec;
	vector<asteroid*> asteroid_rem;

	vector<bullet*>   bullet_vec;
	vector<bullet*>	bullet_rem;

	vector<explosion*> explosion_vec;
	vector<explosion*> explosion_rem;

	void drawAxis(float);
	bool collision(gameEntity*, gameEntity*);
	void createNewAsteroid();
	void createNewExplosion(vectorPosition newPos);
	void drawGameInfo();
	void buildSmallerAsteroids(vectorPosition tempPosition, size tempSize);
	void resetGame();
};
#endif
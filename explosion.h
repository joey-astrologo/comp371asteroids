#ifndef explosion_H
#define explosion_H
#include <vector>

#include "gameEntity.h"
#include "randomNumbers.h"

using std::vector;
using namespace randomNumbers;

const int MAX_PARTICLES = 100;

class explosion : public gameEntity
{
public:
	explosion();
	explosion(vectorPosition);
	~explosion();

	void draw();
	void update(int, bool*);

	bool killExp() { return kill; }

private:
	vector<particle> expParticles;

	bool kill;
};

#endif
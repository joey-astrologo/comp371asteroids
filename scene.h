#ifndef scene_H
#define scene_H

//#include <gl\glew.h>     
#include <GL\freeglut.h>
#include <gl\glu.h>	

#include "word3D.h"

class scene
{
public:
	scene(){};
	~scene(){};

	virtual void draw() = 0;
	virtual void update(int,bool*) = 0;
};
#endif
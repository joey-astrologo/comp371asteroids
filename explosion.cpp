#include "explosion.h"
#include "randomNumbers.h"
#include <math.h>

explosion::explosion()
{

}
explosion::explosion(vectorPosition initPos)
{
	for(int i = 0; i < MAX_PARTICLES; i++)
	{
		particle curParticle;
		curParticle.life = 1000;
		curParticle.age = 0;
		curParticle.color.red = 1.0f;
		curParticle.color.green = 1.0f;
		curParticle.color.blue = 1.0f;
		curParticle.pos = initPos;

		float r = 1.0f;
		float x = getRandomNumf(1.0f, -1.0f);
		float y = sqrt(r - pow(x, 2.0f));

		if(getRandomNum(2, 0) == 0)
		{
 			y = -y;
		}
		curParticle.velocity.x =  x * 0.05f * getRandomNumf(1.0f, 0.0f);
   		curParticle.velocity.y = y * 0.05f * getRandomNumf(1.0f, 0.0f);
		expParticles.push_back(curParticle);
	}
	kill = false;

	Mix_PlayChannel( -1, sndExplosion, 0 );
}
explosion::~explosion()
{

}

void explosion::draw()
{
	glPushMatrix();
		glBegin(GL_LINES);
			for(vector<particle>::iterator i = expParticles.begin(); i != expParticles.end(); i++)
			{
 				glColor3f((*i).color.red, (*i).color.green, (*i).color.blue);
				
				float x1 = (*i).pos.x - (*i).velocity.x * 500.0f;
				float y1 = (*i).pos.y - (*i).velocity.y * 500.0f;

				float x2 = (*i).pos.x;
				float y2 = (*i).pos.y;

				glVertex3f(x1, y1, 0.0f);
				glVertex3f(x2 , y2, 0.0f);
			}
		glEnd();
	glPopMatrix();
}
void explosion::update(int gameTime, bool* /*keyStates*/)
{
	for(int i = 0; i < MAX_PARTICLES; i++)
	{
		//update position
		expParticles[i].pos.x += gameTime * expParticles[i].velocity.x;
		expParticles[i].pos.y += gameTime * expParticles[i].velocity.y;

		expParticles[i].age += gameTime;

		float deltaAge = (float)expParticles[i].age/(float)expParticles[i].life;
		expParticles[i].color.green = 1.56f - deltaAge*2;
		expParticles[i].color.blue = 1.0f - deltaAge*2;

		expParticles[i].velocity.x -= expParticles[i].velocity.x * deltaAge;
		expParticles[i].velocity.y -= expParticles[i].velocity.y * deltaAge;
	}

	if(expParticles[0].age >= expParticles[0].life)
	{
		kill = true;
	}
}



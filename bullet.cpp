#include "bullet.h"

bullet::bullet(int x, int y, float direction) : lifeTimeCounter(0), killBullet(false)
{
	position.x = x;
	position.y = y;
	bSphere.x = 0.0f;
	bSphere.y = 0.0f;
	bSphere.z = 0.0f;
	bSphere.radius = 1;
	degree = direction;

	Mix_PlayChannel( -1, sndBullet, 0 );
}

void bullet::draw()
{
	glPushMatrix();
		glTranslatef(position.x, position.y, 0.0f);
		glRotatef(degree, 0.0f, 0.0f, 1.0f);
		glColor3f(0.0f, 1.0f, 1.0f);
		glRectf(0.0f, 0.0f, 1.0f, -1.0f);
	glPopMatrix();
}
void bullet::update(int gameTime, bool* /*keyStates*/)
{
	float moveSpeed = 0.1f;

	vectorPosition direction;
	direction.x = cos(degree * (PI/180));
	direction.y = sin(degree * (PI/180));

	//update position
	position.x += gameTime * moveSpeed * direction.x;
	position.y += gameTime * moveSpeed * direction.y;

	bSphere.x = position.x;
	bSphere.y = position.y;

	lifeTimeCounter += gameTime;
	if(lifeTimeCounter >= LIFE_TIME)
	{
		killBullet = true;
	}
}
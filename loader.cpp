#include <iostream>

#include <fstream>
#include <vector>
#include <sstream>
#include <locale>
#include <cstring>
#include <Windows.h>
#include <gl\freeglut.h>
#include "loader.h"

using std::vector;
using std::string;

loader::loader(string filename){

	std::ifstream in(filename, std::ios::in);
	if (!in){
		std::cerr << "Cannot open file:" << filename << std::endl;
		exit(EXIT_FAILURE);
	}

	vector<float> *v        = new vector<float>();
	vector<float> *n        = new vector<float>();
	vector<float> *t        = new vector<float>();
	vector<unsigned int> *f = new vector<unsigned int>();

	string line;
	while (std::getline(in, line)){
		if (line.substr(0, 2) == "v ") {
			// vertext
			std::istringstream s(line.substr(2));
			float i;
			s >> i; //x
			v->push_back(i);
			s >> i; //y
			v->push_back(i);
			s >> i; //z
			v->push_back(i);
		}
		else if (line.substr(0, 2) == "vt") {
			// texture
			std::istringstream s(line.substr(2));
			float i;
			s >> i; //u
			t->push_back(i);
			s >> i; //v
			t->push_back(i);
			s >> i; //w
			//t->push_back(i);			
		}
		else if (line.substr(0, 2) == "vn") {
			// normal
			std::istringstream s(line.substr(2));
			float i;
			s >> i; //x
			n->push_back(i);
			s >> i; //y
			n->push_back(i);
			s >> i; //z
			n->push_back(i);
		}
		else if (line.substr(0, 2) == "f ") {
			// face
			std::istringstream s(line.substr(2));
			string str;
			int val;
			while (s.good()) {
				char c = s.get();
				if (std::isdigit(c, s.getloc())){
					str += c;
				}
				else if (c == '/' || std::isspace(c, s.getloc())){
					std::istringstream parser(str);
					parser >> val;
					f->push_back(--val);
					str.clear();
					val = 0;
				}
				else{}
			}
		}
		else { /* ignoring this line */ }
	}

	int i;
	index_num = f->size() / 3;
	v_index = new int[index_num];
	n_index = new int[index_num];
	t_index = new int[index_num];
	for (i = 0; i < index_num; i++){
		v_index[i]   = f->at(i * 3);
		t_index[i] = f->at(i * 3 + 1);
		n_index[i] = f->at(i * 3 + 2);
	}

	vertices_num = v->size();
	vertices = new float[vertices_num];
	for (i = 0; i < vertices_num; i++){
		vertices[i] = v->at(i);
	}

	normal_num = n->size();
	normals = new float[normal_num];
	for (i = 0; i < normal_num; i++){
		normals[i] = n->at(i);
	}

	texture_num = t->size();
	texture = new float[texture_num];
	for (i = 0; i < texture_num; i++){
		texture[i] = t->at(i);
	}

	delete(v); delete(t); delete(n); delete(f);
}

loader::~loader()
{
	delete (this->vertices);
	delete (this->texture);
	delete (this->normals);
	delete (this->v_index);
	delete (this->n_index);
	delete (this->t_index);
}

GLuint loader::loadTexture(std::string filename){
	unsigned int texture;
	unsigned char *data, *fileh, *infoh;
	size_t width, height;

	BITMAPFILEHEADER* bmpHeader;
	BITMAPINFOHEADER* bmpInfo;

	// open texture data
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	if (!in){
		std::cerr << "Cannot open file:" << filename << std::endl;
		exit(EXIT_FAILURE);
	}

	fileh = new unsigned char[sizeof(BITMAPFILEHEADER)];
	infoh = new unsigned char[sizeof(BITMAPINFOHEADER)];
	in.read((char*) fileh, sizeof(BITMAPFILEHEADER));
	in.read((char*) infoh, sizeof(BITMAPINFOHEADER));

	bmpHeader = (BITMAPFILEHEADER*)fileh;
	bmpInfo = (BITMAPINFOHEADER*)infoh;

	if (bmpHeader->bfType != 0x4D42)
	{
		std::cout << "File \"" << filename << "\" isn't a bitmap file\n";
		return 2;
	}

	data = new unsigned char[bmpInfo->biSizeImage];
	

	// Go to where image data starts, then read in image data
	in.seekg(bmpHeader->bfOffBits);
	in.read((char*)data, bmpInfo->biSizeImage);

	unsigned char tmpRGB = 0; // Swap buffer
	for (unsigned long i = 0; i < bmpInfo->biSizeImage; i += 3)
	{
		tmpRGB = data[i];
		data[i] = data[i + 2];
		data[i + 2] = tmpRGB;
	}

	width = bmpInfo->biWidth;
	height = bmpInfo->biHeight;

	glGenTextures(1, &texture);             // Generate a texture
	glBindTexture(GL_TEXTURE_2D, texture); // Bind that texture temporarily

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	glBindTexture(GL_TEXTURE_2D, NULL);

	// Output a successful message
	//std::cout << "Texture \"" << filename << "\" successfully loaded.\n";

	// Delete the two buffers.
	//delete(data);
	delete(fileh);
	delete(infoh);
	in.close();

	return texture; // Return success code 
}
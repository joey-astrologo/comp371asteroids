#ifndef displayText_H
#define displayText_H

#include <GL\freeglut.h>
#include <iostream>
#include <sstream>

using std::string;

namespace displayText
{
	void enter2D();
	void leave2D();
	void writeText(void *, string, int, int);
}

#endif


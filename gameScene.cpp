#include "gameScene.h"
#include "starsBackground.h"

#include <iostream>
#include <sstream>

//using std::string;
using std::stringstream;

gameScene::gameScene() : timer(0), score(0), gameOver(false), gamePlayTimer(500), currentAsteroid(NULL), spawnTime(0), collisionCount(0), pauseShot(200), time(0)
{
	ship = new mainShip;
	background = new starsBackground();
	one = new word3D("resources\\one.dat", 0.0f, 1.0f, 0.0f);
	two = new word3D("resources\\two.dat", 0.0f, 1.0f, 0.0f);
	three = new word3D("resources\\three.dat", 0.0f, 1.0f, 0.0f);
	start = new word3D("resources\\start.dat", 0.0f, 1.0f, 0.0f);
	gameOverW = new word3D("resources\\gameOver.dat", 0.0f, 1.0f, 0.0f);
}
gameScene::~gameScene()
{
	delete background;
	delete ship;
	if(currentAsteroid != NULL)
		delete currentAsteroid;
	delete one;
	delete two;
	delete three;
	delete start;
	for (vector<asteroid*>::iterator cur = asteroid_vec.begin(); cur != asteroid_vec.end(); cur++)
	{
		delete *cur;
	}
	for (vector<bullet*>::iterator cur = bullet_vec.begin(); cur != bullet_vec.end(); cur++)
	{
		delete *cur;
	}
	for (vector<explosion*>::iterator cur =explosion_vec.begin(); cur != explosion_vec.end(); cur++)
	{
		delete *cur;
	}
	asteroid_rem.clear();
	bullet_rem.clear();
	explosion_rem.clear();
}
bool gameScene::collision(gameEntity* obj1, gameEntity* obj2)
{
	double temp = (pow((obj1->bSphere.x - obj2->bSphere.x), 2)) + (pow((obj1->bSphere.y - obj2->bSphere.y),2)) +
		           (pow((obj1->bSphere.z - obj2->bSphere.z), 2));
	double d = sqrt(temp);
	if(d <= obj1->bSphere.radius + obj2->bSphere.radius)
		return true;
	else
		return false;
}
void gameScene::drawAxis(float length)
{
	glPushMatrix();
		glBegin(GL_LINES);
			glVertex3f(-length, 0.0, 0.0);
			glVertex3f(length, 0.0, 0.0);
		glEnd();
		glBegin(GL_LINES);
			glVertex3f(0.0, -length, 0.0);
			glVertex3f(0.0, length, 0.0);
		glEnd();
		glBegin(GL_LINES);
			glVertex3f(0.0, 0.0, -length);
			glVertex3f(0.0, 0.0, length);
		glEnd();
	glPopMatrix();
}
void gameScene::draw()
{
	background->draw();
	if(time <= 5000)
	{
		if(time <= 1000)
		{
			one->drawWord();
		}
		else if(time <= 2000)
		{
			two->drawWord();
		}
		else if(time <= 3000)
		{
			three->drawWord();
		}
		else if(time <= 4000)
		{
			glPushMatrix();
				glTranslatef(-10.0, 0.0, 0.0);
				start->drawWord();
			glPopMatrix();
		}
	}
	else if(!gameOver)
	{
		ship->draw();

		for (vector<asteroid*>::iterator cur = asteroid_vec.begin(); cur != asteroid_vec.end(); cur++)
		{
			(*cur)->draw();
		}

		for (vector<bullet*>::iterator cur = bullet_vec.begin(); cur != bullet_vec.end(); cur++)
		{
			(*cur)->draw();
		}

		for (vector<explosion*>::iterator cur = explosion_vec.begin(); cur != explosion_vec.end(); cur++)
		{
			(*cur)->draw();
		}
	}
	else
	{
		
		glTranslatef(-25.0, 0.0, 0.0);
		glScalef(2.0f, 2.0f, 2.0f);
		gameOverW->drawWord();
	}

	//glColor3f(1.0f, 0.0f, 0.0f);
	//drawAxis(150);

	//temp to show collision
	drawGameInfo();			//for debugging
}
void gameScene::drawGameInfo()
{

	glPushMatrix();
		enter2D();

		stringstream output;
		output << score;

		glColor3f(1.0f, 0.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18,"Score: ", 20, 60);
		glColor3f(0.0f, 1.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18, output.str(), 120, 60);

		glColor3f(1.0f, 0.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18,"Time: ", 1040, 60);
	
		output.str(std::string());
		output << gamePlayTimer;

		glColor3f(0.0f, 1.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18, output.str(), 1160, 60);

		glColor3f(1.0f, 0.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18,"Lives X ", 20, 680);

		output.str(std::string());
		output << ship->lives;

		glColor3f(0.0f, 1.0f, 0.0f);
		writeText(GLUT_BITMAP_HELVETICA_18, output.str(), 90, 680);


		leave2D();
	glPopMatrix();
}
void gameScene::update(int gameTime, bool* keyStates)
{
	time += gameTime;
	if(gamePlayTimer <= 0 || ship->lives <= 0)
	{
		gameOver = true;
	}
	//check for collisions here
	if(time > 5000 && !gameOver)
	{
		for (vector<asteroid*>::iterator cur = asteroid_vec.begin(); cur != asteroid_vec.end(); cur++)
		{
			if((*cur)->collidable())
			{
				if(collision(ship, (*cur)) == true)
				{
					(*cur)->setDirection(-1.0f);
					(*cur)->collided();
					if(ship->collidable())
					{
						ship->collided();
						ship->lives--;
					}
				}
			}
			for (vector<asteroid*>::iterator cur2 = asteroid_vec.begin(); cur2 != asteroid_vec.end(); cur2++)
			{
				if((*cur) != (*cur2) && (*cur)->collidable() && (*cur2)->collidable())
				{
					if(collision((*cur), (*cur2)) == true)
					{
						(*cur)->setDirection(-1.0f);
						(*cur)->collided();
						(*cur2)->setDirection(-1.0f);
						(*cur2)->collided();
						collisionCount++;
					}
				}
			}
			if(bullet_vec.size() >= 1)	
			{
				for (vector<bullet*>::iterator cur2 = bullet_vec.begin(); cur2 != bullet_vec.end(); cur2++)
				{
					if(collision((*cur), (*cur2)) == true)
					{
						asteroid_rem.push_back((*cur));
						bullet_rem.push_back((*cur2));
						createNewExplosion((*cur)->position);
						score += 100;
					}
				}
			}
		}

		//create new game objects
		spawnTime += gameTime;
		if((spawnTime >= 800) && (asteroid_vec.size() < MAX_ASTEROIDS))
		{
			spawnTime = 0;
			createNewAsteroid();
		}

		timer += gameTime;
		if(timer >= 1000)
		{
			gamePlayTimer -= 1;
			timer = 0;
		}
	
		pauseShot += gameTime;
		if(pauseShot >= 200)
		{
			if(keyStates[' '] == true)
			{
				currentBullet = new bullet(ship->getPosition().x, ship->getPosition().y, ship->getDirection());
				bullet_vec.push_back(currentBullet);
				pauseShot = 0;
			}
		
		}
	
		//update objects
		ship->update(gameTime, keyStates);

		for (vector<asteroid*>::iterator cur = asteroid_vec.begin(); cur != asteroid_vec.end(); cur++)
		{
			(*cur)->update(gameTime, keyStates);
		}

		for (vector<bullet*>::iterator cur = bullet_vec.begin(); cur != bullet_vec.end(); cur++)
		{
			(*cur)->update(gameTime, keyStates);
			if((*cur)->getKillState() == true)
			{
				bullet_rem.push_back((*cur));
			}
		}

		for (vector<explosion*>::iterator cur = explosion_vec.begin(); cur != explosion_vec.end(); cur++)
		{
			(*cur)->update(gameTime, keyStates);
			if((*cur)->killExp() == true)
			{
				explosion_rem.push_back((*cur));
			}
		}

		//remove dead objects
		if(bullet_rem.size() >= 1)
		{
			for (vector<bullet*>::iterator cur = bullet_rem.begin(); cur != bullet_rem.end(); cur++)
			{
				vector<bullet*>::iterator iter = find(bullet_vec.begin(), bullet_vec.end(), (*cur));
		
				if(iter != bullet_vec.end())
				{
					bullet* temp = (*iter);
					bullet_vec.erase(iter);
					delete temp;
				}
		
			}
		}
		if(explosion_rem.size() >= 1)
		{
			for (vector<explosion*>::iterator cur = explosion_rem.begin(); cur != explosion_rem.end(); cur++)
			{
				vector<explosion*>::iterator iter = find(explosion_vec.begin(), explosion_vec.end(), (*cur));
		
				if(iter != explosion_vec.end())
				{
					explosion* temp = (*iter);
					explosion_vec.erase(iter);
					delete temp;
				}
			}
		}
		if(asteroid_rem.size() >= 1)
		{
			for (vector<asteroid*>::iterator cur = asteroid_rem.begin(); cur != asteroid_rem.end(); cur++)
			{
				vector<asteroid*>::iterator iter = find(asteroid_vec.begin(), asteroid_vec.end(), (*cur));
				if(iter != asteroid_vec.end())
				{
					asteroid* temp = (*iter);
					vectorPosition tempPosition = temp->getPosition();
					size tempSize = temp->getSize();
					asteroid_vec.erase(iter);
					delete temp;
		
					buildSmallerAsteroids(tempPosition, tempSize);
				}
		
			}
		}
		explosion_rem.clear();
		bullet_rem.clear();
		asteroid_rem.clear();
		//for debugging
		if(keyStates['k'] == true)
		{
			asteroid_vec.clear();
			bullet_vec.clear();
		}
	}
	if(gameOver)
	{
		if(keyStates[13] == true)
		{
			resetGame();
			keyStates[13] == false;
		}
	}
}
void gameScene::resetGame()
{
	delete ship;
	if(currentAsteroid != NULL)
		delete currentAsteroid;
	
	asteroid_rem.clear();
	bullet_rem.clear();
	explosion_rem.clear();
	asteroid_vec.clear();
	bullet_vec.clear();
	explosion_vec.clear();

	ship = new mainShip;
	timer = 0;
	score = 0; 
	gameOver = false;
	gamePlayTimer = 500;
	currentAsteroid = NULL;
	spawnTime = 0;
	pauseShot = 800;  
	time = 0;
}
void gameScene::createNewAsteroid()
{
	currentAsteroid = new asteroid(getRandomNum(MAX_X_AXIS * 2, MAX_X_AXIS * -1), 
									getRandomNum(MAX_Y_AXIS * 2, MAX_Y_AXIS * -1), BIG);
	asteroid_vec.push_back(currentAsteroid);
}
void gameScene::createNewExplosion(vectorPosition newPos)
{
	currentExplosion = new explosion(newPos);
	explosion_vec.push_back(currentExplosion);
}
bool gameScene::setup()
{
	ship = new mainShip;
	background = new starsBackground();
	return true;
}
void gameScene::buildSmallerAsteroids(vectorPosition tempPosition, size tempSize)
{
	int offset = 0;
	int tempDirection = 0;
	//really ugly sorry
	if(tempSize == BIG)
	{
				
			do
			{
				tempDirection = getRandomNum(MAX_DEGREE, 30);
			}while(tempDirection > 150 && tempDirection < 210);
			offset = getRandomNum(8, 3);
			currentAsteroid = new asteroid(tempPosition.x, tempPosition.y + offset, MEDIUM, tempDirection);
			asteroid_vec.push_back(currentAsteroid);

			do
			{
				tempDirection = getRandomNum(MAX_DEGREE, 30);
			}while(tempDirection > 150 && tempDirection < 210);
			currentAsteroid = new asteroid(tempPosition.x + offset, tempPosition.y, MEDIUM, -tempDirection);
			asteroid_vec.push_back(currentAsteroid);

			do
			{
				tempDirection = getRandomNum(MAX_DEGREE, 30);
			}while(tempDirection > 150 && tempDirection < 210);
			currentAsteroid = new asteroid(tempPosition.x, tempPosition.y - offset, MEDIUM, tempDirection * offset);
			asteroid_vec.push_back(currentAsteroid);
	}
	else if(tempSize == MEDIUM)
	{
			offset = getRandomNum(8, 3);
			do
			{
				tempDirection = getRandomNum(MAX_DEGREE, 30);
			}while(tempDirection > 150 && tempDirection < 210);
			currentAsteroid = new asteroid(tempPosition.x, tempPosition.y + offset, SMALL, tempDirection);

			do
			{
				tempDirection = getRandomNum(MAX_DEGREE, 30);
			}while(tempDirection > 150 && tempDirection < 210);
			asteroid_vec.push_back(currentAsteroid);
			currentAsteroid = new asteroid(tempPosition.x + offset, tempPosition.y, SMALL, -tempDirection);
			asteroid_vec.push_back(currentAsteroid);
	}
}
